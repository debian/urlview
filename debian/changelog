urlview (0.9-24) unstable; urgency=medium

  * Goodbye urlview! Set maintainer field to QA Group.

 -- Emanuele Rocca <ema@debian.org>  Mon, 04 Sep 2023 15:06:08 +0200

urlview (0.9-23.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Replace CI config with central one.
  * Convert to 3.0 source format.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set debhelper-compat version in Build-Depends.

 -- Bastian Germann <bage@debian.org>  Sat, 03 Dec 2022 20:02:23 +0100

urlview (0.9-23) unstable; urgency=medium

  [ Emanuele Rocca ]
  * d/url_handler.sh: properly use nohup or suppress STDERR.
    Thanks Daniel Reichelt for the patch. Closes: #933388
  * d/README.Debian: do not mention mimedecode, the package has been removed a
    while ago. Closes: #693785
  * Allow dumping URLs to stdout by setting the URLVIEW_DEBUG environment
    variable. Thanks Francois Lallart. Closes: #598508
  * Fix warning about implicit declaration of function 'quote'.
    Thanks Joshua Root for the patch.
  * Enable Salsa CI Pipeline.
  * Add autopkgtests.
  * d/control: set Standards-Version to 4.6.1.

  [ наб ]
  * d/url_handler.sh: do not require bash for no reason, shellcheck-clean
  * d/README.Debian: align with reality w.r.t. url_handler.sh
    Closes: #1017562
  * d/control: depend on libcurses-dev directly
  * Link against libncursesw, fixing display of non-ASCII URLs
    Closes: #1017569

 -- Emanuele Rocca <ema@debian.org>  Sat, 20 Aug 2022 22:17:46 +0200

urlview (0.9-22) unstable; urgency=medium

  [ Emanuele Rocca ]
  * Move to dh and dh-autoreconf Closes: #993875
  * Drop build dependencies on: linuxdoc-tools, cdbs, libtool, automake1.11,
    autoconf, automake
  * Invoke AM_INIT_AUTOMAKE with "foreign"
  * Set debhelper compatibility level to 13
  * Add debian/source/format
  * Do not ship SGML and HTML version of the man page, remove
    debian/htmlworkaround and doc-base control file
  * Drop debian/watch file, ftp.mutt.org does not have any upstream tarballs
    anymore

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces

 -- Emanuele Rocca <ema@debian.org>  Thu, 16 Sep 2021 12:02:11 +0200

urlview (0.9-21) unstable; urgency=medium

  * Use aclocal-1.16. Closes: #906513
  * url_handler.sh uses sensible-browser, add dependency on sensible-utils.
  * Set debhelper compatibility level to 10.
  * Add Vcs-{Browser,Git} poiting to salsa.
  * Update Standards-Version to 4.2.0.

 -- Emanuele Rocca <ema@debian.org>  Sun, 19 Aug 2018 15:00:03 +0200

urlview (0.9-20) unstable; urgency=medium

  * Invoke automake with --add-missing and bump DEB_AUTO_UPDATE_ACLOCAL to
    1.15. Closes: #818411
  * Build-depend on automake.
  * Pass -n to gzip to allow reproducible builds.
  * Set Standards-Version to 3.9.7.

 -- Emanuele Rocca <ema@debian.org>  Thu, 24 Mar 2016 11:40:48 +0100

urlview (0.9-19) unstable; urgency=low

  * Acknowledge NMU. Thanks, Matt Kraai!
  * Alpine added to mailto_prgs. Closes: #521049
  * Mimedecode removed from "Suggests". Closes: #550885, #597426
  * ncftp2 has been renamed to ncftp. "Suggests" changed accordingly.
  * Standards-Version bumped to 3.9.1:
    - Added {misc:Depends} to "Depends".
    - Debhelper compatibility level set to 8, build-depend on debhelper >= 8.
    - Extra leading whitespaces removed from doc-base control file.
    - Removed debian/postinst as calling install-docs is not required anymore.
    - Fixed debian/NEWS syntax errors.
    - doc-base section changed to Text.

 -- Emanuele Rocca <ema@debian.org>  Sat, 12 Mar 2011 20:42:01 +0100

urlview (0.9-18.1) unstable; urgency=low

  * Non-maintainer upload.
  * Change DEB_AUTO_UPDATE_ACLOCAL from 1.10 to 1.11 and build-depend on
    automake1.11 instead of automake. Closes: #543023

 -- Matt Kraai <kraai@debian.org>  Tue, 29 Sep 2009 06:08:26 -0700

urlview (0.9-18) unstable; urgency=low

  * Build-depend on automake instead of automake1.8. Closes: #474078
  * BROWSER convention url updated in README. Closes: #438953
  * Updated Standards-Version.

 -- Emanuele Rocca <ema@debian.org>  Sun, 06 Apr 2008 11:00:07 +0200

urlview (0.9-17) unstable; urgency=low

  * Use sensible-browser rather than www-browser in url_handler.sh.
  * Updated Standards-Version.

 -- Emanuele Rocca <ema@debian.org>  Sun, 29 Jul 2007 15:41:17 +0200

urlview (0.9-16) unstable; urgency=low

  * Wrapping disabled by default.
    Added WRAP configuration option. Closes: #327850
  * Debian REGEXP and COMMAND hardcoded in urlview.c Closes: #322432
  * Switched from simple-patchsys (cdbs) to darcs-buildpackage:
     - debian/patches/ directory removed
     - call to simple-patchsys.mk removed
  * Standards-Version updated
  * Address of the FSF updated in debian/copyright

 -- Emanuele Rocca <ema@debian.org>  Wed, 05 Oct 2005 18:35:52 +0200

urlview (0.9-15) unstable; urgency=low

  * Patched to support url pre-selection.
    Thanks to Kim Oldfield for the patch. Closes: #108581

 -- Emanuele Rocca <ema@debian.org>  Thu,  9 Jun 2005 12:40:24 +0200

urlview (0.9-14) experimental; urgency=low

  * Patched to support wrapping.
    Thanks to Martin Michlmayr for the patch. Closes: #307576

 -- Emanuele Rocca <ema@debian.org>  Sun, 05 Jun 2005 15:07:33 +0200

urlview (0.9-13) experimental; urgency=low

  * Missing build dependencies added: libtool, automake1.8, autoconf.
    Thanks, Kenshi Muto. Closes: #309169
  * Patched to support page up and page down also with few results.
    Closes: #307577
  * Changed 'Recommends' to: elinks | www-browser to make lintian happy.

 -- Emanuele Rocca <ema@debian.org>  Sun, 15 May 2005 12:37:24 +0200

urlview (0.9-12) experimental; urgency=low

  * Switched to CDBS
    - debian/rules rewritten to support CDBS
    - changes from the pristine tarball splitted and added to debian/patches
    - debhelper versioned build-dependency bumped to 4.1.0
  * Watch file added

 -- Emanuele Rocca <ema@debian.org>  Wed,  4 May 2005 21:49:24 +0200

urlview (0.9-11) unstable; urgency=low

  * debian/url_handler.sh:
    Added support for RFC 2368 urls (mailto URL scheme).
    Closes:#233066

 -- Emanuele Rocca <ema@debian.org>  Mon, 16 Feb 2004 14:36:09 +0100

urlview (0.9-10) unstable; urgency=low

  * Avoid segfaulting if /dev/tty is unreadable.
    Thanks to Jim Paris <jim@jtan.com> for the patch. Closes:#224560

 -- Emanuele Rocca <ema@debian.org>  Mon, 22 Dec 2003 14:26:54 +0100

urlview (0.9-9) unstable; urgency=low

  * Handling of ftp file urls does not break anymore.
    url_handler.sh ask now to the user what to do if the ftp url could be
    releated to a file. Closes:#219532
  * Updated maintainer field. Joey, thanks for sponsoring!

 -- Emanuele Rocca <ema@debian.org>  Sun, 16 Nov 2003 15:41:54 +0100

urlview (0.9-8) unstable; urgency=low

  * Manpage updated in order to reflect  the change introduced in 0.9-7.
    Closes:#218214
  * Cosmetic changes to debian/rules
  * Sponsored by Joey Hess <joeyh@debian.org>

 -- Emanuele Rocca <emarocca@libero.it>  Wed, 29 Oct 2003 21:46:15 +0100

urlview (0.9-7) unstable; urgency=low

  * Added support for file-urls, through the following changes:
    - debian/url_handler.sh: added the 'file_prgs' programs tipology, with wget
      and snarf as associated programs
    - debian/control: Added Suggests: wget | snarf
    Closes:#188051
  * urlview.c: applied the patch from Viktor Rosenfeld (thanks, Viktor) that
    allows to ignore the BROWSER environment variable if COMMAND is present in
    the rc-file. Closes:#212789
  * debian/rules: switched to debhelper
  * Updated Standard-versions to 3.6.1
  * Sposored by Joey Hess <joeyh@debian.org>

 -- Emanuele Rocca <emarocca@libero.it>  Tue, 07 Oct 2003 21:03:07 +0200

urlview (0.9-6) unstable; urgency=low

  * Added Suggests: mimedecode in debian/control.
    Added more information on how to handle mime-encoded input in
    README.Debian. Closes: #128183, #134013
  * README.Debian slightly modified.
  * Sponsored by Joey Hess <joeyh@debian.org>.

 -- Emanuele Rocca <emarocca@libero.it>  Tue, 16 Sep 2003 14:47:13 +0200

urlview (0.9-5) unstable; urgency=low

  * Enhanced support for urls without protocol, like
    www.debian.org/social_contract.
  * Added support for ftp urls without protocol.
  * Sponsored by Joey Hess <joeyh@debian.org>.

 -- Emanuele Rocca <emarocca@libero.it>  Fri, 13 Jun 2003 19:53:11 +0200

urlview (0.9-4) unstable; urgency=low

  * Fixed the problem of zero-length $BROWSER environment variable.
    Thanks to Kevin Kreamer <kkreamer@etherogz.org> for the fine patch.
    Closes: #183439
  * Added support for "bare" urls (like www3.bad.example.com). Closes: #148722
  * Installed urlview-1.html in /usr/share/doc/urlview/html. Closes: #189258
  * Small changes to url_handler.sh, as suggested by Martin Schulze
    <joeyh@infodrom.org>. Closes: #123963
  * Fixed newlines and carriage-returns problems. Closes: #183440, #144219
  * Minor changes to debian/rules.
  * Sponsored by Joey Hess <joeyh@debian.org>.

 -- Emanuele Rocca <emarocca@libero.it>  Mon, 26 May 2003 20:59:54 +0200

urlview (0.9-3) unstable; urgency=low

  * New maintainer. Closes: #194035
  * Changed error message for single quote carachter in $BROWSER environment
    variable. Closes: #176251
  * Correctly remove /etc/urlview on purge. Closes: #160039
  * Updated Standards-Version to 3.5.10
  * Sponsored by Joey Hess <joeyh@debian.org>.

 -- Emanuele Rocca <emarocca@libero.it>  Fri, 23 May 2003 18:08:32 +0200

urlview (0.9-2.2) unstable; urgency=low

  * NMU for upcoming Debian browser policy compliance:
  * Added support for BROWSER environment variable, with a patch from ESR,
     - modified to escape the url properly even with BROWSER, to prevent
       malformed urls from running commands in the shell
     - modified so BROWSER overrides the COMMAND in the urlview files
     - modified to call strtok correctly
     - and strtok a temp buffer, not the command, so that multiple urls can be
       viewed in a single invocation without always running the first browser
     - modify sgml docs
  * Also added x-www-browser and www-browser to it, tried first.
  .
  * While I'm at at it:
  * Added all the current day common X and text mode browsers to
    debian/url_handler.sh. Closes: #169677, #92896
  * Completed FHS transition by removing /usr/doc symlink code.
  * Don't ship a .dhelp file in the package; let doc-base create it.
    Closes: #60027
  * Remove debian/substvars on clean.

 -- Joey Hess <joeyh@debian.org>  Thu,  9 Oct 2003 15:34:21 -0400

urlview (0.9-2.1) unstable; urgency=low

  * NMU with maintainer permission.
  * debian/README.Debian: corrected spelling error (developped -> developed)
  * debian/control: added Build-Depends
  * debian/control: updated Standards-Version to 3.5.6
  * debian/copyright: fixed location of GPL (Closes: Bug#79751)
  * debian/copyright: cleaned up
  * debian/doc-base_urlview: document id corrected (Closes: Bug#85451)
  * debian/rules: work around for Bug#90870 (<HTML> missing from urlview.html)
  * debian/rules: added -isp to dpkg-gencontrol
  * debian/url_handler.sh: use x-terminal-emulator (Closes: Bug#74736)

 -- Edward Betts <edward@debian.org>  Thu,  2 Aug 2001 22:26:35 +0100

urlview (0.9-2) unstable; urgency=low

  * Corrected copyright file info (Closes #66791)
 -- Luis Francisco Gonzalez <luisgh@debian.org>  Fri,  7 Jul 2000 22:56:01 +0100

urlview (0.9-1) unstable; urgency=low

  * New upstream source

 -- Luis Francisco Gonzalez <luisgh@debian.org>  Tue,  4 Jul 2000 22:56:31 +0100

urlview (0.7-9) unstable; urgency=low

  * Added new tag to url_handler.sh to allow X-programs to be spawn with
    nohup (Fixes #60196, #64981)
  * Compiled against ncurses (Fixes #64981)
  * Applied patch to remove repeated URLs (Fixes #64982)
  * Removed bashisms from debian/rules (Fixes #64983)

 -- Luis Francisco Gonzalez <luisgh@debian.org>  Tue, 13 Jun 2000 21:31:46 +0100

urlview (0.7-8) frozen unstable; urgency=low

  * Fixed typo in prerm (Bug#55222) (Bug#55519)

 -- Luis Francisco Gonzalez <luisgh@debian.org>  Tue, 18 Jan 2000 23:19:12 +0100

urlview (0.7-7) unstable; urgency=low

  * Changed REGEXP for URLs (Bug#27023)
  * Compressed changelogs (Bug#35376)
  * Netscape enabled by default (Bug#42984)
  * Added support of doc-base (Bug#31182)

 -- Luis Francisco Gonzalez <luisgh@debian.org>  Mon, 20 Sep 1999 22:34:36 +0000

urlview (0.7-6.1) unstable; urgency=medium

  * NMU: recompiled with slang 1.2.2-3 to fix dependencies.

 -- Jim Mintha <jmintha@debian.org>  Fri,  3 Sep 1999 00:34:36 +0200

urlview (0.7-6) frozen unstable; urgency=high

  * Recompiled with slang1 (Fixes Bug#28799)

 -- Luis Francisco Gonzalez <luisgh@debian.org>  Mon,  2 Nov 1998 09:48:53 +0000

urlview (0.7-5) unstable; urgency=low

  * Changed the regular expresion to remove trailing brakets and colons.
    (Fixes Bug#25144)

 -- Luis Francisco Gonzalez <luisgh@debian.org>  Sat,  1 Aug 1998 15:56:40 +0100

urlview (0.7-4) unstable; urgency=low

  * Added the new url_handler.sh, a slightly modified version of that
    contributed by Martin Schulze <joey@debian.org>. Please check
    README.debian for configuration. (Fixes Bug#23189)
  * Added support for https to the url catcher in system.urlview. (Fixes
    Bug#23261)

 -- Luis Francisco Gonzalez <luisgh@debian.org>  Fri, 12 Jun 1998 11:30:17 +0100

urlview (0.7-3) unstable; urgency=low

  * Removed file INSTALL from the binary: Fixes #19232.

 -- Luis Francisco Gonzalez <luisgh@debian.org>  Fri, 13 Mar 1998 15:11:36 +0000

urlview (0.7-2) unstable; urgency=low

  * Compressed manual page.

 -- Luis Francisco Gonzalez <luisgh@debian.org>  Fri, 27 Feb 1998 14:04:35 +0000

urlview (0.7-1) unstable; urgency=low

  * Initial Release.

 -- Luis Francisco Gonzalez <luisgh@debian.org>  Fri, 20 Feb 1998 15:40:31 +0000
